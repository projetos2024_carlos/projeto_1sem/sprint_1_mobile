import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { NavigationContainer } from '@react-navigation/native';
import PageInit from './src/pageInit';
import Login from './src/login';
import Cadastro from './src/cadastro'
import Home from './src/home'
import AlugarChurrasco from './src/hiring/alugarChurrasco'
import AlugarFesta from './src/hiring/alugarFesta'
import AlugarPiscina from './src/hiring/alugarPiscina'

const Stack = createStackNavigator();

export default function Routes() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="PageInit" screenOptions={{ headerShown: false }}>
        <Stack.Screen name="Home" component={Home} />
        <Stack.Screen name="Login" component={Login} />
        <Stack.Screen name="Cadastro" component={Cadastro} />
        <Stack.Screen name="PageInit" component={PageInit} />
        <Stack.Screen name="alugarChurrasco" component={AlugarChurrasco} />
        <Stack.Screen name="alugarFesta" component={AlugarFesta} />
        <Stack.Screen name="alugarPiscina" component={AlugarPiscina} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
