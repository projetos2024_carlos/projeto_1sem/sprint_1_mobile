import React, { useState } from "react";
import { StatusBar } from "expo-status-bar";
import { TextInput, StyleSheet, Text, View, ScrollView, TouchableOpacity, Image, Alert } from "react-native";
import logoBV from '../assets/logoBV.png'
import { useNavigation } from '@react-navigation/native'; // Importando o hook useNavigation
import sheets from "./axios/axios";

export default function Cadastro() {
  const navigation = useNavigation(); // Obtendo a instância de navegação
  const [user, setUser] = useState({
    email:'',
    password:'',
    name:'',
    ap:'',
    confirmPassword:'',
    telephone:'',
  });
  const [email, setEmail] = useState('');
  const [nome, setNome] = useState('');
  const [ap, setAp] = useState('');
  const [senha, setSenha] = useState('');
  const [telefone, setTelefone] = useState('');
  const [confirmarSenha, setConfirmarSenha] = useState('');

  const handleCadastro = async () => {
    if (!email || !nome || !ap || !senha || !confirmarSenha || !telefone) {
      Alert.alert('Preencha todos os campos');
    } else if (senha !== confirmarSenha) {
      Alert.alert('As senhas não coincidem');
    } else {
      try {
        const response = await sheets.postUser(user);
        if (response.status === 200) {
          Alert.alert(response.data.message);
          navigation.navigate('Home');
        } 
      } catch (error) {
        Alert.alert(error.response.data.message);
      }
    }
  };//cadastro

  return (
    <View style={styles.container}>

      <Image source={logoBV} style={styles.imagem} />
      <View style={styles.inputsContainer}>
        <TextInput
          style={styles.input}
          placeholder="Email"
          onChangeText={setEmail}
          value={email}
        />{/* input email */}
        <TextInput
          style={styles.input}
          placeholder="Nome"
          onChangeText={setNome}
          value={nome}
        />{/* input Nome */}
        <TextInput
          style={styles.input}
          placeholder="AP"
          onChangeText={setAp}
          value={ap}
        />{/* input AP */}
        <TextInput
          style={styles.input}
          placeholder="Telefone"
          onChangeText={setTelefone}
          value={telefone}
        />{/* input AP */}
        <TextInput
          style={styles.input}
          placeholder="Senha"
          secureTextEntry={true}
          onChangeText={setSenha}
          value={senha}
        />{/* input senha */}
        <TextInput
          style={styles.input}
          placeholder="Confirmar Senha"
          secureTextEntry={true}
          onChangeText={setConfirmarSenha}
          value={confirmarSenha}
        />{/* input Confirma Senha */}
      </View>
      <View style={styles.buttonContainer}>
        <TouchableOpacity style={styles.button} onPress={handleCadastro}>
          <Text style={styles.buttonText}>Cadastrar</Text>
        </TouchableOpacity>{/* botão cadastrar */}
      </View>
      <StatusBar style="false" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#efd5c4",
  },
  inputsContainer: {
    width: "80%",
    marginBottom: 5,
  },
  input: {
    height: 40,
    borderWidth: 1,
    borderColor: "black",
    borderRadius: 20,
    paddingHorizontal: 10,
    paddingLeft: 10,
    backgroundColor: "#E7DDDA",
    marginBottom: 5,
    opacity: 0.8
  },
  buttonContainer: {
    width: "60%",
    marginBottom: 100,
    marginTop: 20,
  },
  button: {
    backgroundColor: "#E7DDDA",
    borderRadius: 10,
    padding: 10,
    marginBottom: 4,
    alignItems: "center",
    borderColor: "black",
    borderWidth: 1,
  },
  buttonText: {
    color: "black",
    fontWeight: "bold",
  },
  imagem: {
    height: "40%",
    width: "100%",
    marginTop: -50,
    marginBottom: 40,
  },
  errorMessage: {
    color: 'black',
    marginBottom: 10,
  }
});
