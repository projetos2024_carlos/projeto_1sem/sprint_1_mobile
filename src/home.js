import { StatusBar } from "expo-status-bar";
import { StyleSheet, Text, View, ScrollView } from "react-native";
import { useNavigation } from '@react-navigation/native';
import ProductList from './productList';

export default function Home() {
  const navigation = useNavigation();

  return (
    <View style={styles.container}>
      <StatusBar/>
      <View style={styles.content}>
        <ScrollView>
          <Text style={styles.textTitle}>Áreas Disponíveis</Text>
          <ProductList/>
        </ScrollView>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    justifyContent: "space-between",
  },
  header: {
    height: 50,
    backgroundColor: "#efd5c4",
    justifyContent: "center"
  },

  content: {
    flex: 1,
    backgroundColor: "#efd5c4",
  },

  footer: {
    height: 50,
    backgroundColor: "#efd5c4",
  },
  text: {
    fontSize: 16,
    fontWeight: "bold",
    textAlign: "center",
  },
  textTitle: {
    marginTop: 16,
    fontWeight: "bold",
    fontSize: 24,
    textAlign: 'center',
  },
  
});