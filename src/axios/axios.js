import axios from 'axios';
const api = axios.create({
    baseURL: "http://10.89.234.174:5000/sistema/",
    headers: {
        'accept': 'application/json',
    },
});

const sheets = {

    postUser:(user)=>api.post("cadastro/",user),
    getUsers:()=>api.get("getuser/"),
    updateUser:(user)=>api.put("updateuser/",user),
    deleteUser:(_id)=>api.delete("deleteuser/"+_id),

    postLogin:(user)=>api.post("login/",user),
    //getLogin:(_id)=>api.get("login/"+_id),

    getTables:()=>api.get("getTables/"),
    getTablesDesc:()=>api.get("getDescTable/"),
    
    postArea:(area)=>api.post("areapost/",area),
    getArea:()=>api.get("areaget/"),
    getAreaId:(_id)=>api.get("areagetId/"+_id),
    updateArea:(_id)=>api.put("areaput/"+_id),
    deleteArea:(_id)=>api.delete("areadel/"+_id),
}

export default sheets;