import React, {useState} from 'react';
import { View, Text, TouchableOpacity, Modal, StyleSheet } from 'react-native';

const times = ['10:00', '12:00', '14:00', '16:00', '18:00', '20:00'];

const AlugarFesta = () => {
  const [selectedTime, setSelectedTime] = useState(null);
  const [modalVisible, setModalVisible] = useState(false);

  const handleReserve = (time) => {
    setSelectedTime(time);
    setModalVisible(true);
  };

  const handleModalOk = () => {
    setModalVisible(false);
    navigation.navigate('pageInit', { message: `Você reservou esse espaço para a hora ${selectedTime}` });
  };

    return (
        <View style={styles.container}>
          <Text style={styles.title}>Salão de Festas</Text>
          {times.map((time) => (
        <View key={time} style={styles.timeContainer}>
          <Text style={styles.timeText}>{time}</Text>
          <TouchableOpacity style={styles.button} onPress={() => handleReserve(time)}>
            <Text style={styles.buttonText}>Reserve</Text>
          </TouchableOpacity>
        </View>
      ))}
      <Modal animationType="fade" transparent={true} visible={modalVisible}>
        <View style={styles.modalContainer}>
          <View style={styles.modalContent}>
            <Text style={styles.modalText}>Você reservou esse espaço para a hora {selectedTime}</Text>
            <TouchableOpacity style={styles.modalButton} onPress={handleModalOk}>
              <Text style={styles.modalButtonText}>Ok</Text>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
        </View>
      );
    };
    
    const styles = StyleSheet.create({
      container: {
        flex: 1,
        padding: 16,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#efd5c4',
      },
      title: {
        fontSize: 24,
        fontWeight: 'bold',
        marginBottom: 16,
      },
      description: {
        fontSize: 16,
        marginBottom: 16,
      },
      content: {
        flex: 1,
        backgroundColor: "#efd5c4",
      },
      timeContainer: {
        width: '60%',
        height: '15%',
        backgroundColor: '#E7DDDA',
        marginBottom: 20,
        justifyContent: 'center',
        alignItems: 'center',
      },
      timeText: {
        fontSize: 20,
        fontWeight: 'bold',
      },
      button: {
        backgroundColor: '#E7DDDA',
        padding: 10,
        borderRadius: 5,
        marginTop: 10,
        width: '70%',
        borderColor: "black",
        borderWidth: 1,
      },
      buttonText: {
        textAlign: 'center',
        color: 'black',
        fontSize: 16,
      },
      modalContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'rgba(0, 0, 0, 0.1)',
      },
      modalContent: {
        width: '80%',
        backgroundColor: 'white',
        padding: 20,
        borderRadius: 10,
        alignItems: 'center',
      },
      modalText: {
        fontSize: 18,
        fontWeight: 'bold',
        marginBottom: 20,
      },
      modalButton: {
        backgroundColor: '#E7DDDA',
        padding: 10,
        borderRadius: 5,
        width: '80px',
      },
      modalButtonText: {
        color: 'black',
        fontSize: 16,
        textAlign: 'center',
       },
    });

export default AlugarFesta;