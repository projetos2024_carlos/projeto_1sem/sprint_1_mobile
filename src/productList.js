import React from 'react';
import { View, Text, Image, StyleSheet, TouchableOpacity } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import Churrasqueira from '../assets/churrasqueira.png';
import Festa from '../assets/festas.png'
import Piscina from '../assets/piscina.png'


const productListData = [
  { id: "Churrasco", name: 'Área de Churrasco', imagem: Churrasqueira, description: 'Uma área perfeita para churrasco com os amigos.' },
  { id: "Piscina", name: 'Área da Piscina', imagem: Piscina, description: 'Uma área completa com piscina, espreguiçadeira, mesas e cadeiras para o seu lazer.' },
  { id: "Festa", name: 'Salão de Festas', imagem: Festa, description: 'Um salão de festas espaçoso e elegante, perfeito para qualquer evento.' },
];

const ProductList = () => {
  const navigation = useNavigation();

  return (
    <View style={styles.container}>
      {productListData.map((product) => (
        <View key={product.id} style={styles.productContainer}>
          <View style={{ flexDirection: 'row' }}>
            <Image source={product.imagem} style={styles.image} />
            <View style={styles.descriptionContainer}>
              <Text style={styles.description}>{product.description}</Text>
              <TouchableOpacity
                style={styles.button}
                onPress={() => navigation.navigate(`alugar${product.id}`)}
              >
                <Text style={styles.buttonText}>Verificar Horários</Text> 
              </TouchableOpacity>
            </View>
          </View>
        </View>
      ))}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 16,
  },
  productContainer: {
    marginTop: 40,
  },
  image: {
    maxHeight: 160,
    maxWidth: 160,
    borderRadius: 8,
    marginBottom: 8,
  },
  descriptionContainer: {
    marginLeft: 16,
    flex: 1,
  },
  description: {
    fontSize: 18,
    fontWeight: 'normal',
    marginBottom: 8,
    maxWidth: 200,
  },
  name: {
    fontSize: 18,
    fontWeight: 'bold',
    marginBottom: 4,
  },
  button: {
    backgroundColor: "#E7DDDA",
    borderRadius: 10,
    padding: 10,
    marginBottom: 4,
    maxWidth: 200,
    marginTop: 40,
    alignItems: "center",
    borderColor: "black",
    borderWidth: 1,
  },
  buttonText: {
    color: '#001',
    fontSize: 16,
    fontWeight: 'bold',
    textAlign: 'center',
  },
});

export default ProductList;